FROM golangci/golangci-lint:v1.45.0-alpine
# hadolint ignore=DL3018
RUN apk --no-cache add git jq bash
COPY /assets/.golangci.yml /golangci/.golangci.yml
